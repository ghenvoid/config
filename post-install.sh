#!/bin/bash

##SETTING PACMAN
sed -i '/#Color/c\Color' /etc/pacman.conf
sed -i '/ParallelDownloads/c\ParallelDownloads = 10' /etc/pacman.conf
sed -i '/#\[multilib]/a\Include = /etc/pacman.d/mirrorlist' /etc/pacman.conf
sed -i '/#\[multilib]/c\[multilib]' /etc/pacman.conf
until pacman -Syu --noconfirm --needed reflector
do
	echo 'Pacman failed, trying again.'
done
reflector --verbose --threads "$(nproc --all)" --protocol https --latest 20 --sort rate --save /etc/pacman.d/mirrorlist

##VARIABLES DECLARATION
while :
	do
		read -r -p 'BIOS or UEFI Bootloader? (bios/uefi) Type "none" to none of those: ' bios_uefi
		case "$bios_uefi" in
			[Bb][Ii][Oo][Ss])
				read -r -p '(Where do you want to install the GRUB Bootloader? (/dev/sdX): ' block_device
				bios_uefi=bios
				break
				;;
			[Uu][Ee][Ff][Ii])
				bios_uefi=uefi
				break
				;;
			[Nn])
				bios_uefi=n
				break
				;;
			*)
				;;
		esac
done
read -r -p 'Define the hostname: ' my_hostname
read -r -p 'Define your username: ' my_username
read -r -p "Define the root's password: " my_rootp
read -r -p "Define your user's password: " my_userp
while :
do
	read -r -p 'Desktop Environment or Base install?:
base; kde; gnome; xfce; lxqt;
: ' de_wm
	case "$de_wm" in
		[Bb][Aa][Ss][Ee])
			de_wm=base
			break
			;;
		[Kk][Dd][Ee])
			de_wm=kde
			break
			;;
		[Gg][Nn][Oo][Mm][Ee])
			de_wm=gnome
			break
			;;
		[Xx][Ff][Cc][Ee])
			de_wm=xfce
			break
			;;
		[Ll][Xx][Qq][Tt])
			de_wm=lxqt
			break
			;;
		*)
			;;
	esac
done

##BASE CONFIG
ln -sf /usr/share/zoneinfo/America/Recife /etc/localtime
hwclock --systohc
while :
do
	read -r -p 'Portugues ou/or English? (p/e): ' my_locale
	case "$my_locale" in
		[Pp])
			sed -i '/#pt_BR.UTF-8 UTF-8/c\pt_BR.UTF-8 UTF-8' /etc/locale.gen
			sed -i '/#pt_BR ISO-8859-1/c\pt_BR ISO-8859-1' /etc/locale.gen
			locale-gen
			echo 'LANG=pt_BR.UTF-8' > /etc/locale.conf
			break
			;;
		[Ee])
			sed -i '/#en_US.UTF-8 UTF-8/c\en_US.UTF-8 UTF-8' /etc/locale.gen
			sed -i '/#en_US ISO-8859-1/c\en_US ISO-8859-1' /etc/locale.gen
			locale-gen
			echo 'LANG=en_US.UTF-8' > /etc/locale.conf
			break
			;;
		*)
			;;
	esac
done
echo "$my_hostname" > /etc/hostname
echo "127.0.0.1	localhost
::1		localhost
127.0.1.1	$my_hostname.localdomain	$my_hostname" > /etc/hosts

##ADDITIONAL CONFIG
useradd -mG wheel "$my_username"
echo root:"$my_rootp" | chpasswd && echo "Root's password set!"
echo "$my_username":"$my_userp" | chpasswd && echo "User's password set!"
chmod 640 /etc/sudoers
sed -i '/# %wheel ALL=(ALL) ALL/c\ %wheel      ALL=(ALL) ALL' /etc/sudoers
chmod 440 /etc/sudoers
timedatectl set-ntp true
sleep 3

until pacman -Syu --noconfirm --needed aria2 bash-completion dosfstools flatpak git gnu-free-fonts intel-media-driver inter-font libva-intel-driver libva-mesa-driver mesa mesa-vdpau mtools neovim networkmanager noto-fonts noto-fonts-emoji ntfs-3g os-prober pipewire pipewire-alsa pipewire-media-session pipewire-pulse sudo ttf-hack ttf-liberation ttf-linux-libertine xdg-user-dirs xdg-utils
do
	echo "Pacman failed, trying again."
	sleep 3
done
while :
do
	case "$bios_uefi" in
		[Bb][Ii][Oo][Ss])
			until pacman -Syu --noconfirm --needed grub
			do
				echo 'Pacman failed, trying again.'
			done
			grub-install --target=i386-pc "$block_device"
			echo 'GRUB_DISABLE_OS_PROBER=false' >> /etc/default/grub
			grub-mkconfig -o /boot/grub/grub.cfg
			break
			;;
		[Uu][Ee][Ff][Ii])
			until pacman -Syu --noconfirm --needed refind
			do
				echo 'Pacman failed, trying again.'
			done
			mkdir -p /boot/efi/EFI/BOOT
			mkdir -p /boot/efi/EFI/refind/drivers_x64
			cp -v /usr/share/refind/refind_x64.efi /boot/efi/EFI/BOOT/bootx64.efi
			cp -v /usr/share/refind/drivers_x64/* /boot/efi/EFI/refind/drivers_x64/
			break
			;;
		[Nn])
			break
			;;
		*)
			;;
	esac
done
while :
do
	case "$de_wm" in
		[Bb][Aa][Ss][Ee])
			break
			;;
		[Kk][Dd][Ee])
			until pacman -Syu --noconfirm --needed xorg plasma plasma-wayland-session dolphin konsole packagekit-qt5 firefox qbittorrent
			do
				echo "Pacman failed, trying again."
				sleep 3
			done
			systemctl enable NetworkManager
			systemctl enable sddm
			break
			;;
		[Gg][Nn][Oo][Mm][Ee])
			until pacman -Syu --noconfirm --needed xorg gnome gnome-software-packagekit-plugin firefox
			do
				echo "Pacman failed, trying again."
				sleep 3
			done
			systemctl enable NetworkManager
			systemctl enable gdm
			break
			;;
		[Xx][Ff][Cc][Ee])
			until pacman -Syu --noconfirm --needed xorg xfce4 xfce4-goodies network-manager-applet gnome gnome-software-packagekit-plugin sddm firefox
			do
				echo "Pacman failed, trying again."
				sleep 3
			done
			systemctl enable NetworkManager
			systemctl enable sddm
			break
			;;
		[Ll][Xx][Qq][Tt])
			until pacman -Syu --noconfirm --needed xorg lxqt network-manager-applet pavucontrol-qt breeze qbittorrent papirus-icon-theme breeze-icons discover packagekit-qt5 gvfs gvfs-mtp sddm firefox
			do
				echo "Pacman failed, trying again."
				sleep 3
			done
			systemctl enable NetworkManager
			systemctl enable sddm
			break
			;;
		*)
			echo 'Error: Doing base only install.'
			sleep 5
			break
			;;
	esac
done
systemctl enable reflector.timer

echo ''
echo 'Done. Exit, swapoff and umount -a.'
