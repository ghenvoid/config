#!/bin/bash

read -r -p 'Type "bios" or "uefi" for bios boot or uefi boot respectively; press "n" for none of those: ' bios_uefi
while :
	do
		case "$bios_uefi" in
			bios)
				read -r -p 'Where do you want to install the GRUB Bootloader? (/dev/xyz): ' block_device
				break
				;;
			uefi)
				break
				;;
			n)
				break
				;;
			*)
				;;
		esac
done
read -r -p 'Define the hostname: ' my_hostname
read -r -p 'Define your username: ' my_username
read -s -r -p 'Define your roots password: ' my_rootp
echo ''
read -s -r -p 'Define your users password: ' my_userp
echo ''
read -r -p 'Portugues(p)/English(e): ' my_locale
read -r -p 'Desktop Environment or Base install?:
base; kde; gnome; xfce; lxqt;
: ' de_wm

ln -sf /usr/share/zoneinfo/America/Recife /etc/localtime
hwclock --systohc

while :
	do
		case "$my_locale" in
			p)
				echo 'pt_BR.UTF-8 UTF-8' >> /etc/locale.gen
				locale-gen
				echo 'LANG=pt_BR.UTF-8' >> /etc/locale.conf
				break
				;;
			e)
				echo 'en_US.UTF-8 UTF-8' >> /etc/locale.gen
				locale-gen
				echo 'LANG=en_US.UTF-8' >> /etc/locale.conf
				break
				;;
			*)
				;;
		esac
done

echo "$my_hostname" >> /etc/hostname
echo "hostname='$my_hostname'" >> /etc/conf.d/hostname
echo "127.0.0.1	localhost
::1		localhost
127.0.1.1	$my_hostname.localdomain	$my_hostname" >> /etc/hosts

echo "root:$my_rootp" | chpasswd
echo "$my_username:$my_userp" | chpasswd


pacman -S --noconfirm --needed artix-archlinux-support
echo '[lib32]
Include = /etc/pacman.d/mirrorlist

#ARCHLINUX
[extra]
Include = /etc/pacman.d/mirrorlist-arch

[community]
Include = /etc/pacman.d/mirrorlist-arch

[multilib]
Include = /etc/pacman.d/mirrorlist-arch
' >> /etc/pacman.conf


pacman -S --noconfirm --needed grub os-prober networkmanager-openrc neovim git xdg-utils xdg-user-dirs pipewire pipewire-pulse pipewire-alsa pipewire-media-session ntfs-3g dosfstools mtools ttf-linux-libertine ttf-hack noto-fonts noto-fonts-emoji ttf-liberation gnu-free-fonts inter-font aria2 openntpd-openrc

while :
	do
		case "$de_wm" in
			base)
				break
				;;
			kde) 
				pacman -S --noconfirm --needed xorg plasma plasma-wayland-session dolphin konsole packagekit-qt5 libva-intel-driver intel-media-driver firefox qbittorrent
				rc-update add NetworkManager
				rc-update add sddm
				break
				;;
			gnome)
				pacman -S --noconfirm --needed xorg gnome gnome-software-packagekit-plugin gdm-openrc libva-intel-driver intel-media-driver firefox
				rc-update add NetworkManager
				rc-update add gdm
				break
				;;
			xfce)
				pacman -S --noconfirm --needed xorg xfce4 xfce4-goodies network-manager-applet sddm-openrc libva-intel-driver intel-media-driver firefox
				rc-update add NetworkManager
				rc-update add sddm
				break
				;;
			lxqt)
				pacman -S --noconfirm --needed xorg lxqt network-manager-applet pavucontrol-qt breeze qbittorrent papirus-icon-theme breeze-icons gvfs gvfs-mtp sddm-openrc libva-intel-driver intel-media-driver firefox
				rc-update add NetworkManager
				rc-update add sddm
				break
				;;
			*)
				;;
		esac
done

while :
	do
		case "$bios_uefi" in
			bios)
				grub-install --target=i386-pc "$block_device"
				echo 'GRUB_DISABLE_OS_PROBER=false' >> /etc/default/grub
				grub-mkconfig -o /boot/grub/grub.cfg
				break
				;;
			uefi)
				pacman -S refind
				mkdir -p /boot/efi/EFI/BOOT
				mkdir -p /boot/efi/EFI/refind/drivers_x64
				cp /usr/share/refind/refind_x64.efi /boot/efi/EFI/BOOT/bootx64.efi
				cp /usr/share/refind/drivers_x64/drivername_x64.efi /boot/efi/EFI/refind/drivers_x64/
				break
				;;
			n)
				break
				;;
			*)
				;;
		esac
done

rc-update add openntpd
useradd -mG wheel "$my_username"
echo '%wheel      ALL=(ALL) ALL' >> /etc/sudoers

echo ''
echo 'Done, swapoff and umount -a now.'
